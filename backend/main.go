package main

import (
	"log"
	"os"

	"gitlab.com/eemj/spotify-dashboard/backend/server"
)

var requiredEnvs = []string{
	"SPOTIFY_ID",
	"SPOTIFY_SECRET",
}

func main() {
	for _, env := range requiredEnvs {
		if os.Getenv(env) == "" {
			log.Fatalf("Missing '%s' variable.", env)
		}
	}

	if port := os.Getenv("PORT"); port == "" {
		err := os.Setenv("PORT", "8081")

		if err != nil {
			log.Fatalln(err)
		}
	}

	conn := server.DBConnection{
		Host:     os.Getenv("DATABASE_HOST"),
		Port:     os.Getenv("DATABASE_PORT"),
		User:     os.Getenv("DATABASE_USER"),
		Password: os.Getenv("DATABASE_PASS"),
		DBName:   os.Getenv("DATABASE_NAME"),
	}

	srv, err := server.NewServer(conn)

	if err != nil {
		log.Fatalln(err)
	}

	defer srv.DB.Close()

	err = srv.Run()

	if err != nil {
		log.Fatalln(err)
	}
}
