package controllers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"gitlab.com/eemj/spotify-dashboard/backend/models"
)

const (
	SpotifyUserInfo        string = "https://api.spotify.com/v1/me"
	SpotifyCurrentPlayback string = "https://api.spotify.com/v1/me/player"
	SpotifyPausePlayback   string = "https://api.spotify.com/v1/me/player/pause"
	SpotifyStopPlayback    string = "https://api.spotify.com/v1/me/player/stop"
	SpotifyPlayPlayback    string = "https://api.spotify.com/v1/me/player/play"
	SpotifyVolumePlayback  string = "https://api.spotify.com/v1/me/player/volume"
	SpotifySeekPlayback    string = "https://api.spotify.com/v1/me/player/seek"
	SpotifyNextPlayback    string = "https://api.spotify.com/v1/me/player/next"
	SpotifyPrevPlayback    string = "https://api.spotify.com/v1/me/player/previous"
)

type Artist struct {
	Name string `json:"name"`
}

type Image struct {
	Height uint16 `json:"height"`
	Width  uint16 `json:"width"`
	URL    string `json:"url"`
}

type Album struct {
	Artists []Artist `json:"artists"`
	Images  []Image  `json:"images"`
}

type Item struct {
	Album       Album  `json:"album"`
	Name        string `json:"name"`
	TrackNumber uint   `json:"track_number"`
	Duration    uint64 `json:"duration_ms"`
}

type Playing struct {
	Timestamp uint64 `json:"timestamp"`
	Progress  uint64 `json:"progress_ms"`
	IsPlaying bool   `json:"is_playing"`
	Type      string `json:"currently_playing_type"`
	Item      Item   `json:"item"`
}

type UserInfo struct {
	Country string  `json:"country"`
	Name    string  `json:"display_name"`
	Email   string  `json:"email"`
	Product string  `json:"product"`
	Images  []Image `json:"images"`
}

type PlaybackMethod struct {
	Method string
	URI    string
	Path   string
}

var Methods = []PlaybackMethod{
	PlaybackMethod{
		Method: http.MethodPut,
		URI:    SpotifyPausePlayback,
		Path:   "/pause",
	},
	PlaybackMethod{
		Method: http.MethodPut,
		URI:    SpotifyPlayPlayback,
		Path:   "/play",
	},
	PlaybackMethod{
		Method: http.MethodPost,
		URI:    SpotifyNextPlayback,
		Path:   "/next",
	},
	PlaybackMethod{
		Method: http.MethodPost,
		URI:    SpotifyPrevPlayback,
		Path:   "/prev",
	},
}

func Playback(uri, method string, r interface{}) (res *http.Response, body []byte, err error) {
	var accessToken string
	switch r.(type) {
	case *http.Request:
		accessToken = r.(*http.Request).Context().Value("user").(*models.User).AccessToken
	case string:
		accessToken = r.(string)
	}

	req, err := http.NewRequest(method, uri, nil)
	req.Header.Add("Authorization", "Bearer "+accessToken)

	if err != nil {
		return
	}

	if res, err = http.DefaultClient.Do(req); err != nil {
		return
	}

	defer res.Body.Close()

	body, err = ioutil.ReadAll(res.Body)

	return
}

func CheckStatusCode(r *http.Response, err *error) {
	switch r.StatusCode {
	default:
	case http.StatusNoContent:
		break
	case http.StatusNotFound:
		(*err) = errors.New("No active device")
		break
	case http.StatusForbidden:
		(*err) = errors.New("Premium required")
		break
	}
}

func GetUserInfo(accessToken string) (info *UserInfo, err error) {
	res, body, err := Playback(SpotifyCurrentPlayback, http.MethodGet, accessToken)

	if err != nil {
		return
	}

	if res.StatusCode == http.StatusOK {
		info = new(UserInfo)
		err = json.Unmarshal(body, info)
	} else {
		err = errors.New("Unauthorized access")
	}

	return
}

func CurrentPlaying(r *http.Request) (play *Playing, err error) {
	res, body, err := Playback(SpotifyCurrentPlayback, http.MethodGet, r)

	if err != nil {
		return
	}

	if res.StatusCode == http.StatusNoContent {
		return
	}

	play = new(Playing)
	err = json.Unmarshal(body, play)

	return
}
