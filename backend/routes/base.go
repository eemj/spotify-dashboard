package routes

import "net/http"

func (s *Routes) pingHandler(w http.ResponseWriter, r *http.Request) {
	s.ResJSON(w, http.StatusOK, "Pong")
}

func (s *Routes) notFoundHandler(w http.ResponseWriter, r *http.Request) {
	s.ResError(w, http.StatusNotFound, "Route not found.")
}

func (s *Routes) notAllowedHandler(w http.ResponseWriter, r *http.Request) {
	s.ResError(w, http.StatusMethodNotAllowed, "Method not allowed!")
}

func (s *Routes) robotsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("User-agent: *\nDisallow: /"))
	w.WriteHeader(http.StatusOK)
}
