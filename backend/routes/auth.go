package routes

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/jinzhu/gorm"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/spotify"

	"gitlab.com/eemj/spotify-dashboard/backend/controllers"
	"gitlab.com/eemj/spotify-dashboard/backend/models"
)

var (
	scopes = []string{
		"user-read-email",
		"user-read-playback-state",
		"user-modify-playback-state",
	}
	spotifyOauthConfig = &oauth2.Config{
		ClientID:     os.Getenv("SPOTIFY_ID"),
		ClientSecret: os.Getenv("SPOTIFY_SECRET"),
		Endpoint:     spotify.Endpoint,
		Scopes:       scopes,
	}
	ignoredAuth = []string{
		"/auth/spotify",
		"/auth/spotify/callback",
		"/favicon.ico",
		"/robots.txt",
		"/ping",
	}
	ignoredAssets = []string{
		"/css/",
		"/js/",
		"/img/",
	}
)

const (
	sessionDuration     time.Duration = (time.Duration(12) * time.Hour)
	spotifyAuthEndpoint string        = "/auth/spotify"

	spotifyURI          string = ""
	renewAccessTokenURI string = "https://accounts.spotify.com/api/token"
)

type Image struct {
	Height uint16 `json:"height"`
	Width  uint16 `json:"width"`
	URL    string `json:"url"`
}

type TokenResponse struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
}

func RenewAccessToken(refreshToken string) (TokenResponse, error) {
	params := url.Values{}
	params.Set("grant_type", "refresh_token")
	params.Set("refresh_token", refreshToken)
	payload := strings.NewReader(params.Encode())

	req, _ := http.NewRequest(http.MethodPost, renewAccessTokenURI, payload)

	auth := base64.StdEncoding.EncodeToString([]byte(
		fmt.Sprint(os.Getenv("SPOTIFY_ID"), ":", os.Getenv("SPOTIFY_SECRET")),
	))

	req.Header.Add("Authorization", fmt.Sprint("Basic ", auth))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	resp := TokenResponse{}

	err := json.Unmarshal(body, &resp)

	if err != nil {
		return resp, err
	}

	return resp, nil
}

func GenerateRandomString() (str string) {
	buff := make([]byte, 1<<5)
	rand.Read(buff)
	str = base64.StdEncoding.EncodeToString(buff)
	return
}

func (s *Routes) spotifyHandler(w http.ResponseWriter, r *http.Request) {
	randomString := GenerateRandomString()

	if spotifyOauthConfig.RedirectURL == "" {
		spotifyOauthConfig.RedirectURL = fmt.Sprintf(
			"http://localhost:%s/auth/spotify/callback",
			os.Getenv("PORT"),
		)
	}

	url := spotifyOauthConfig.AuthCodeURL(randomString)

	http.SetCookie(w, &http.Cookie{
		Name:    "oas",
		Value:   randomString,
		Expires: time.Now().Add(3 * time.Minute),
	})

	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (s *Routes) spotifyCallbackHandler(w http.ResponseWriter, r *http.Request) {
	state := r.FormValue("state")

	if state == "" {
		s.ResError(w, http.StatusBadRequest, "Invalid request, missing parameters.")
		return
	}

	oas, err := r.Cookie("oas")

	if err != nil || oas == nil {
		s.ResError(w, http.StatusBadRequest, "Unable to retrieve cookies.")
		return
	}

	if state != oas.Value {
		s.ResError(w, http.StatusInternalServerError, "OAuth2 state mismatch.")
		log.Printf("Invalid OAuth2 state, expected %s but got %s.\n", oas.Value, state)
		return
	}

	code := r.FormValue("code")

	token, err := spotifyOauthConfig.Exchange(context.Background(), code)
	if err != nil {
		s.ResError(w, http.StatusInternalServerError, "Code exchange failed.")
		log.Printf("Code exchange failed with %s.\n", err)
		return
	}

	info, err := controllers.GetUserInfo(token.AccessToken)

	if err != nil {
		s.ResError(w, http.StatusInternalServerError, err.Error())
		return
	}

	tokenExpiration := time.Now().Add((time.Duration(1) * time.Hour) - (time.Duration(2) * time.Minute))

	sessionExpiration := time.Now().Add(sessionDuration)

	user := models.User{
		CountryCode:  info.Country,
		Email:        info.Email,
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
		Name:         info.Name,
		Session:      GenerateRandomString(),
		Expiry:       tokenExpiration,
		Premium:      info.Product == "premium",
	}

	if len(info.Images) > 0 {
		user.Avatar = info.Images[0].URL
	}

	sid, err := r.Cookie("sid")

	/*
		Step 1:
		If the user has the cookie check with the database.
			- If the cookie was found within the database, ensure that they're the same email.
				- If they're not the same email, re-generate a new session string and go back to step 1
			- If the cookie was not found in the database, return the cookie to the channel
	*/

	if sid != nil && err == nil {
		currentSession := (*sid).Value

	recheck:
		queryUser := new(models.User)
		if err := s.DB.Where(models.User{Session: currentSession}).Find(&queryUser).Error; err != nil && err == gorm.ErrRecordNotFound {
			(*sid).Value = user.Session
			(*sid).Expires = sessionExpiration
		} else {
			currentSession = GenerateRandomString()
			user.Session = currentSession
			goto recheck
		}

	} else {
		sid = &http.Cookie{
			Name:    "sid",
			Value:   user.Session,
			Expires: sessionExpiration,
		}
	}

	(*sid).HttpOnly = true

	/*
		Step 2:
		If theres a user in the database with the same email
			- If its an expired entry:
				- Renew and assign the sid.Value to the queryUser
		Otherwise create
	*/

	// We might find ourselves, with the user having an email registered but with another session id
	queryUser := new(models.User)
	if err := s.DB.Where(models.User{Email: user.Email}).Find(&queryUser).Error; err != nil && err == gorm.ErrRecordNotFound {
		if err := s.DB.Create(&user).Error; err != nil {
			s.ResError(w, http.StatusBadRequest, "Failed to register session.")
			return
		}
	} else {
		if time.Now().After(queryUser.Expiry) {
			(*queryUser).Expiry = tokenExpiration
			(*queryUser).AccessToken = user.AccessToken
		}

		(*queryUser).Session = (*sid).Value

		if err := s.DB.Save(&queryUser).Error; err != nil {
			s.ResError(w, http.StatusInternalServerError, err.Error())
			return
		}
	}

	// Ensure that this cookie is for everthing.
	(*sid).Path = "/"

	http.SetCookie(w, sid)

	// Delete the cookie, once we're finished using it.
	http.SetCookie(w, &http.Cookie{
		Name:   "oas",
		Value:  "",
		MaxAge: 0,
	})

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

// TODO; Find a way where we won't have to request the database every second, perhaps another
// key-value database, that we'll store the session as the key and the expiration time as the value.
func (s *Routes) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		for _, path := range ignoredAuth {
			if r.URL.Path == path {
				next.ServeHTTP(w, r)
				return
			}
		}

		for _, asset := range ignoredAssets {
			if strings.HasPrefix(r.URL.Path, asset) {
				next.ServeHTTP(w, r)
				return
			}
		}

		sid, err := r.Cookie("sid")

		if err != nil {
			http.Redirect(w, r, spotifyAuthEndpoint, http.StatusTemporaryRedirect)
			return
		}

		user := &models.User{}
		if err := s.DB.Where(models.User{Session: sid.Value}).Find(&user).Error; err == gorm.ErrRecordNotFound {
			http.Redirect(w, r, spotifyAuthEndpoint, http.StatusTemporaryRedirect)
			return
		}

		if time.Now().UnixNano() >= user.Expiry.UnixNano() {
			tokenResponse, err := RenewAccessToken(user.RefreshToken)

			if err != nil {
				s.ResError(w, http.StatusInternalServerError, "Failed to unmarshal the JSON response.")
				return
			}

			expiration := time.Now().Add(
				(time.Duration(tokenResponse.ExpiresIn) * time.Second) -
					(time.Duration(30) * time.Second),
			)

			(*user).Expiry = expiration
			(*user).AccessToken = tokenResponse.AccessToken

			if rows := s.DB.Save(&user).RowsAffected; rows == 0 {
				s.ResError(w, http.StatusInternalServerError, "Failed to renew your session.")
				return
			}
		}

		ctx := context.WithValue(r.Context(), "user", user)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
