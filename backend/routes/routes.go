package routes

import (
	"log"
	"net/http"
	"path/filepath"
	"time"

	"github.com/jinzhu/gorm"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
)

const frontend string = "../frontend/dist/"

type Routes struct {
	DB *gorm.DB
}

func (s *Routes) New() (router *chi.Mux) {
	router = chi.NewRouter()

	corsOptions := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           3600,
	})

	router.Use(
		middleware.RealIP,
		middleware.Timeout(30*time.Second),
		middleware.Logger,
		corsOptions.Handler,
		s.AuthMiddleware,
	)

	for _, path := range []string{
		"/",
		"/js",
		"/css",
		"/img",
	} {
		router.Mount(path, http.StripPrefix(
			path, http.FileServer(http.Dir(filepath.Join(frontend, path))),
		))
	}

	router.Get("/robots.txt", s.robotsHandler)

	router.Get("/ping", s.pingHandler)

	router.Route("/auth", func(r chi.Router) {
		r.Get("/spotify", s.spotifyHandler)
		r.Get("/spotify/callback", s.spotifyCallbackHandler)
	})

	router.Route("/api", func(r chi.Router) {
		r.Get("/user-info", s.currentUserInfo)
		r.Get("/playing", s.currentPlaying)

		s.initPlaybackMethods(&r)
	})

	router.NotFound(s.notFoundHandler)
	router.MethodNotAllowed(s.notAllowedHandler)

	return router
}

func (s *Routes) ResJSON(w http.ResponseWriter, code int, payload interface{}) {
	res := APIResponse{Data: payload}
	jsonPayload, err := res.MarshalJSON()

	if err != nil {
		log.Printf("Error: %s", err.Error())
		s.ResError(w, http.StatusInternalServerError, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(jsonPayload)
}

func (s *Routes) ResError(w http.ResponseWriter, code int, message string) {
	if code == http.StatusInternalServerError {
		if message == "" {
			message = "Internal Server Error"
		} else {
			log.Println(message)
		}
	}

	res := APIResponse{Code: code, Error: message}
	jsonPayload, err := res.MarshalJSON()

	if err != nil {
		log.Printf("Error: %s", err.Error())
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(jsonPayload)
}
