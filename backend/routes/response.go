package routes

import "encoding/json"

type APIResponse struct {
	Error string      `json:"error,omitempty"`
	Code  int         `json:"code,omitempty"`
	Data  interface{} `json:"data,omitempty"`
}

func (r *APIResponse) MarshalJSON() ([]byte, error) {
	if r.Error != "" && r.Data == nil {
		return json.Marshal(struct {
			Error string `json:"error"`
			Code  int    `json:"code"`
		}{
			Error: r.Error,
			Code:  r.Code,
		})
	}
	return json.Marshal(struct {
		Data interface{} `json:"data"`
	}{
		Data: r.Data,
	})
}
