package routes

import (
	"net/http"

	"github.com/go-chi/chi"

	"gitlab.com/eemj/spotify-dashboard/backend/controllers"
	"gitlab.com/eemj/spotify-dashboard/backend/models"
)

func (s *Routes) currentUserInfo(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(*models.User)
	s.ResJSON(w, http.StatusOK, *user)
}

func (s *Routes) currentPlaying(w http.ResponseWriter, r *http.Request) {
	play, err := controllers.CurrentPlaying(r)

	if err != nil {
		s.ResJSON(w, http.StatusInternalServerError, err.Error())
		return
	}

	s.ResJSON(w, http.StatusOK, play)
}

func (s *Routes) templatePlayback(uri, method string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		res, _, err := controllers.Playback(uri, method, r)

		if err != nil {
			s.ResError(w, http.StatusBadRequest, err.Error())
			return
		}

		controllers.CheckStatusCode(res, &err)

		if err != nil {
			s.ResError(w, http.StatusBadRequest, err.Error())
			return
		}

		s.ResJSON(w, http.StatusAccepted, "Ok!")
	})
}

func (s *Routes) initPlaybackMethods(r *chi.Router) {
	for _, item := range controllers.Methods {
		(*r).Put(item.Path, s.templatePlayback(item.URI, item.Method))
	}
}
