package server

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"gitlab.com/eemj/spotify-dashboard/backend/models"
	"gitlab.com/eemj/spotify-dashboard/backend/routes"
)

type Server struct {
	DB     *gorm.DB
	Router *chi.Mux
	Host   string
}

func (s *Server) Run() error {
	return http.ListenAndServe(
		fmt.Sprintf(":%s", os.Getenv("PORT")),
		s.Router,
	)
}

func (s *Server) AutoMigrate() {
	s.DB.AutoMigrate(models.User{})
}

type DBConnection struct {
	Host, Port, User, DBName, Password string
}

func (d DBConnection) connectionString() (string, string) {
	if d.Host != "" && d.User != "" && d.Password != "" && d.DBName != "" && d.Port != "" {
		return "postgres", fmt.Sprintf(
			"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			d.Host, d.Port, d.User, d.Password, d.DBName,
		)
	}
	return "sqlite3", "/tmp/spotify-dashboard.db"
}

func NewServer(connection DBConnection) (server *Server, err error) {
	dialect, host := connection.connectionString()

	db, err := gorm.Open(dialect, host)

	if err != nil {
		return server, err
	}

	server = &Server{
		DB:   db,
		Host: host,
	}

	router := routes.Routes{DB: db}
	server.Router = router.New()

	server.AutoMigrate()

	if len(os.Getenv("DEBUG")) > 0 {
		server.DB.LogMode(true)
	}

	return server, err
}
