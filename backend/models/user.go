package models

import (
	"encoding/json"
	"time"

	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Session      string    `gorm:"column:session;unique;not null" json:"-"`
	Name         string    `gorm:"column:name" json:"name"`
	CountryCode  string    `gorm:"column:country;type:varchar(2)" json:"country_code"`
	Email        string    `gorm:"column:email;unique" json:"email"`
	Avatar       string    `gorm:"column:avatar" json:"avatar"`
	AccessToken  string    `gorm:"column:access_token" json:"-"`
	RefreshToken string    `gorm:"column:refresh_token" json:"-"`
	Expiry       time.Time `gorm:"column:expiry" json:"-"`
	Premium      bool      `gorm:"column:premium" json:"premium"`
}

type DisplayUser struct {
	Premium     bool   `json:"premium"`
	Name        string `json:"name"`
	CountryCode string `json:"country_code"`
	Email       string `json:"email"`
	Avatar      string `json:"avatar"`
}

func (User) TableName() string {
	return "users"
}

func (u *User) Display() DisplayUser {
	return DisplayUser{
		Premium:     u.Premium,
		Name:        u.Name,
		CountryCode: u.CountryCode,
		Email:       u.Email,
		Avatar:      u.Avatar,
	}
}

func (u *User) MarshalJSON() ([]byte, error) {
	return json.Marshal(u)
}
